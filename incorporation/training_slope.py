import sys
sys.path.append('../calina/')
from processing import create_basic_dataset
from processing import create_negative, create_positive, create_x_feature
from processing import BADCALS
from training import read_dataset
import argparse
import pymc3 as pm
import pandas as pd
import os
import numpy as np


if __name__ == "__main__":


	parser = argparse.ArgumentParser(description='Train basic model (intercept)')
	parser.add_argument('dump_path', type=str,
						help='path to dump folder')
	parser.add_argument('--sensor_type', type=str,
						help='sensor type to train (r or phi)')

	args = parser.parse_args()

	dataset = read_dataset()
	basic = create_basic_dataset(dataset, args.sensor_type, BADCALS)
	basic = create_x_feature(basic, BADCALS)
	negative = create_negative(basic, BADCALS)
	xfactor = negative['x']
	del negative['x']
	negative = negative.iloc[:,9:]


	settings={
		'draws': 80000,
		'tune': 20000,
	}
	meancenters = pd.read_csv(os.path.join(args.dump_path,"{}_meancenters.csv".format(args.sensor_type)))
	meansds = pd.read_csv(os.path.join(args.dump_path,"{}_meansds.csv".format(args.sensor_type)))
	with pm.Model() as model:
		x = pm.Uniform('x', 0,40, observed=xfactor[:,np.newaxis])
		k = pm.Uniform('k', -10,10,shape=negative.shape[1])
		m = pm.Uniform('m', -10,10,shape=negative.shape[1])
		observations = pm.Normal("obs", mu=meancenters.values.T[0]+k*x, sd=meansds.values.T[0]+m*x, observed=negative)

	with model:
		step = pm.Metropolis(vars=[k,m, observations])
		# trace = pm.sample(**settings, step=step, njobs=1, chains=1)
		trace = pm.sample(**settings, step=step, chains=1)
	pm.backends.ndarray.save_trace(trace, args.dump_path+'/slope', overwrite=True)
	print("Calculating means")
	meank = trace['k'].mean(axis=0)
	meanm = trace['m'].mean(axis=0)
	pd.DataFrame(meank).to_csv(os.path.join(args.dump_path,"slope/{}_meank.csv".format(args.sensor_type)), index=False)
	pd.DataFrame(meanm).to_csv(os.path.join(args.dump_path,"slope/{}_meanm.csv".format(args.sensor_type)), index=False)
