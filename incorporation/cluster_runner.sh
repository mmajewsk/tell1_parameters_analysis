#!/bin/bash -l
## Nazwa zlecenia
## Liczba alokowanych węzłów
#SBATCH -N 2
## Liczba zadań per węzeł (domyślnie jest to liczba alokowanych rdzeni na węźle)
#SBATCH -n 24
## Ilość pamięci przypadającej na jeden rdzeń obliczeniowy (domyślnie 5GB na rdzeń)
#SBATCH --mem-per-cpu=5GB
## Maksymalny czas trwania zlecenia (format HH:MM:SS)
#SBATCH  
## Nazwa grantu do rozliczenia zużycia zasobów
#SBATCH -A plghawker2018
## Specyfikacja partycji
#SBATCH -p plgrid-gpu
## Plik ze standardowym wyjściem
#SBATCH --output="output_phi.out"
## Plik ze standardowym wyjściem błędów
#SBATCH --error="error_phi.err"
#SBATCH --gres=gpu:2


cd /net/people/plghawker/projects/tell1_parameters_analysis/incorporation2
module add plgrid/apps/cuda/7.5

MKL_THREADING_LAYER=GNU python training.py /net/scratch/people/plghawker/model_dump --sensor_type R
exit