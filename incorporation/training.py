import sys
sys.path.append('../calina/')
from processing import create_basic_dataset
from processing import create_negative, create_positive
from processing import BADCALS
import argparse
import pandas as pd
import os
import pymc3 as pm


from calibration_dataset import Tell1Dataset, DatasetTree

def read_dataset(datasetpath=r'../../../data/calibrations'):
	class MyDS(Tell1Dataset):
		filename_format = '%Y-%m-%d'
		filename_regex_format = r'\d{4}-\d{2}-\d{2}.csv'

	data_list = MyDS.get_filepaths_from_dir(datasetpath)
	dataset = MyDS(data_list, read=True)
	return dataset

if __name__ == "__main__":


	parser = argparse.ArgumentParser(description='Train basic model (intercept)')
	parser.add_argument('dump_path', type=str,
	                    help='path to dump folder')
	parser.add_argument('--sensor_type', type=str,
	                    help='sensor type to train (r or phi)')

	args = parser.parse_args()

	dataset = read_dataset()
	basic = create_basic_dataset(dataset, args.sensor_type, BADCALS)
	positive = create_positive(basic, BADCALS)
	positive = positive.iloc[:,9:]

	settings={
		'draws': 80000,
		'tune': 20000,
	}

	positive.to_csv(os.path.join(args.dump_path,"positive.csv"))

	with pm.Model() as model:
	    sds = pm.Uniform("sds", 0, 0.75, shape=positive.shape[1])
	    centers = pm.Uniform("centers", 0,50, shape=positive.shape[1])
	    observations = pm.Normal("obs", mu=centers, sd=sds, observed=positive)


	with model:
		step = pm.Metropolis(vars=[sds,centers, observations])
		# trace = pm.sample(**settings, step=step, njobs=1, chains=1)
		trace = pm.sample(**settings, step=step, chains=1)

	# pm.backends.text.dump(args.dump_path, trace)
	pm.backends.ndarray.save_trace(trace, args.dump_path+'/tracedump', overwrite=True)

	print("Calculating means")
	meancenters = trace['centers'].mean(axis=0)
	meansds = trace['sds'].mean(axis=0)
	pd.DataFrame(meancenters).to_csv(os.path.join(args.dump_path,"{}_meancenters.csv".format(args.sensor_type)), index=False)
	pd.DataFrame(meansds).to_csv(os.path.join(args.dump_path,"{}_meansds.csv".format(args.sensor_type)), index=False)
