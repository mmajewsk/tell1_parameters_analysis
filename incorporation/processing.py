from dataset import Tell1Dataset, DatasetTree

BADCALS = {
		'2011-03-07':1.,
		'2012-08-02':3.,
		'2012-07-30': 10.,
		'2012-08-01':10.
}

def remove_anomalies(dataset, sensor_type):
	#sensor type: 'R' of 'phi'
	header_list = [0, 32, 64, 96, 159, 191, 223, 255, 287, 319, 351, 383, 415, 447, 479, 511, 543, 575, 607, 639, 640, 672, 704, 736, 768, 800, 832, 864, 896, 928, 960, 992, 1024, 1056, 1088, 1120, 1183, 1215, 1247, 1279, 1311, 1343, 1375, 1407, 1439, 1471, 1503, 1535, 1567, 1599, 1631, 1663, 1664, 1696, 1728, 1760, 1792, 1824, 1856, 1888, 1920, 1952, 1984, 2016]
	header_columns = ["channel{}".format(c) for c in header_list]
	sensor_data = dataset.dfh[sensor_type].df
	no_header_crosstalk = sensor_data.drop(labels=header_columns,axis='columns')
	no_hc_no_anomalies = no_header_crosstalk[((no_header_crosstalk.iloc[:,9:]>=50).sum(axis=1) == 0)]
	return no_hc_no_anomalies

def read_dataset(datasetpath=r'../data/calibrations'):
	dataset = Tell1Dataset(datasetpath)
	return dataset

def create_x_feature(dataset, badcals):
	for dt in dataset.datetime.unique():
	    key = str(dt)[:10]
	    val = badcals.get(key, 0.)
	    dataset.loc[dataset.datetime==dt,'x'] = val
	return dataset

def create_basic_dataset(dataset, sensor_type, badcals):
	no_hc_no_anomalies = remove_anomalies(dataset, sensor_type)
	dataset = create_x_feature(no_hc_no_anomalies, badcals)
	return dataset

def create_negative(basic_dataset, badcals):
	bad_calibration = badcals.keys()
	negative_feature = basic_dataset[basic_dataset['datetime'].isin(bad_calibration)]
	return negative_feature

def create_positive(basic_dataset, badcals):
	bad_calibration = badcals.keys()
	positive = basic_dataset[~basic_dataset['datetime'].isin(bad_calibration)]
	del positive['x']
	return positive

