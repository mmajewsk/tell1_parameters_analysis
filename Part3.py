# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.7.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
import numpy

# %%
# %load_ext autoreload
# %autoreload 2 

# %%
import sys
sys.path.append('../src')

from dataset import Tell1Dataset, DatasetTree
import dataset as DS

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# %matplotlib inline
from ipywidgets import interact
import numpy as np

from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()
import matplotlib
matplotlib.rcParams['image.cmap'] = 'rainbow'

# %% [markdown]
# Read dataset:

# %%
dataset = Tell1Dataset(r'../../data/calibrations')


# %%
def sensor_histogram(data):
    x_data_list = []
    y_data_list = []
    for i,column in enumerate(data):
        y_data = list(data[column].values)
        y_data_list += y_data
        x_data_list += [i]*len(y_data)
    return x_data_list, y_data_list


# %%

# %% [markdown]
# # Thresholds

# %% [markdown]
# Thresholds are used to determine the hit in detector. They are calculated by measuring the standard deviation of signal in channel. High threshold is 6 sigma and low i s 2 sigma (or smth), so 

# %% [markdown]
# The plot below represents the high threshold value in **all calibrations** for **all sensors**, both R-type and Phi-type.

# %%
data = dataset.dfh.df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30])
axe.set_title('All module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% [markdown]
# There are some values that are bumping the scope of the histogram. lets zoom to the bottom part

# %%
data = dataset.dfh.df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]])
axe.set_title('All module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% [markdown]
# we can see that adc oscillates somewhat around value of 10-12. Those data are to general, we need to separate R type sensor and phi type

# %% [markdown]
# ### R-type

# %%
data = dataset.dfh['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]])
axe.set_title('R-type module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% [markdown]
# ### Phi - type

# %%
data = dataset.dfh['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]])
axe.set_title('Phi-type module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% [markdown]
# In both cases we can see that values oscillate around 10, but faint 'ghost-distribution' is visible.

# %% [markdown]
# ## R-type, ghosting

# %% [markdown]
# To determine whats causing this ghosting, lets just inspect entries with values greater than 15.0

# %%
dfhr = dataset.dfh['R'].df
increased = dfhr.iloc[:,9:]>15.0

# %% [markdown]
# Lets check the sum of entriex exceeding 15 per sensor calibration

# %%
increased.sum(axis=1).hist(bins=20, log=True)

# %% [markdown]
# We can see that there are outliers with more than 400 entries that exceed that value, lets separate those entries.

# %%
high_values = dfhr[increased.sum(axis=1)>400]

# %% [markdown]
# And see at what dates did they occur

# %%
bad_calibration = high_values['datetime'].unique()
bad_calibration

# %% [markdown]
# Those are the dates that correspond with callibration process errors.

# %% [markdown]
# lets look at the distributions per channel again, after excluding those dates

# %%
wrong = dataset.dfh.df[dataset.dfh.df['datetime'].isin(bad_calibration)]
other = dataset.dfh.df[~dataset.dfh.df['datetime'].isin(bad_calibration)]
wrong = DS.DatasetTree.from_df(wrong)
other = DS.DatasetTree.from_df(other)

# %% [markdown]
# #### R-type sensors for High threshold

# %% [markdown]
# Wrong

# %%
data = wrong['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(20,5))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]])

# %% [markdown]
# Good

# %%
data = other['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(20,5))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]])

# %% [markdown]
# #### Phi-type for High threshold:

# %% [markdown]
# worng

# %%
data = wrong['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(20,5))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]])

# %% [markdown]
# good

# %%
data = other['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(20,5))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]])

# %% [markdown]
# ## Important !!
#  - We see some faint peculiar distribution in some channels, in both good calibrations and bad
#  - still, there are some high values, that are outside the scope of the plotsabove 

# %% [markdown]
# # Wild things and vampires

# %%
filtered = (other.df.iloc[:,9:]>=50) & (other.df.iloc[:,9:]!=127.0)
outliers = other.df.iloc[:,9:][filtered]

# %%
occurance_per_row = filtered.sum(axis=1)

# %%
chart1_df = other.df.drop(other.df.iloc[:,9:].columns , axis=1)

# %%
chart1_df['occurances'] = occurance_per_row

# %%
axe = chart1_df.groupby(['datetime']).sum().plot(y='occurances',kind='bar')
axe.set_title('Time evolution of number of very high threshold occurances')
axe.set_xlabel('Calib date')
axe.set_ylabel('# of occurances')

# %%
chart2_df = chart1_df[chart1_df.occurances != 0]

# %%
x = chart2_df.datetime
labels = chart2_df['sensor_number'].unique()
todf = {k:[] for k in labels}
yx = []
for date in x:
    for label in labels:
        data_ = chart2_df[chart2_df.datetime==date]
        val = data_[data_.sensor_number==label].occurances.values
        if not val:
            val = 0
        else:
            val = val[0]
        todf[label].append(val)
occurances_df = pd.DataFrame(todf,index=x.values)
occurances_df = occurances_df.sort_index()

# %%

# %%
occurances_df

# %%
occurances_df.to_csv()

# %%
fig, axe = plt.subplots(1,1,figsize=(8,6))
occurances_df.plot(kind='bar', ax=axe,align="center", stacked=True)
plt.gca().xaxis.set_major_formatter(plt.FixedFormatter(occurances_df.index.to_series().dt.strftime("%d-%m-%Y")))
fig.autofmt_xdate()
axe.set_title("Other outliers (x>50 & x!=127)")
axe.set_xlabel('Calibration date')
axe.set_ylabel('Occurances in sensor')

# %%
chart1_df.groupby(['datetime']).sum()

# %%
axe = chart1_df.groupby(['sensor_number']).sum().plot(y='occurances')
axe.set_title('Occurances of very high threshold per sensor')
axe.set_xlabel('Sensor')
axe.set_ylabel('# of occurances')

# %%
persensor = chart1_df.groupby(['sensor_number']).sum()

# %%
persensor[persensor['occurances']!=0]

# %%
data = other[occurance_per_row!=0].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(20,5))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[50,500]])
axe.set_title('Phi-type sensor callibrations where value >=50 and value != 127')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
data = other[occurance_per_row!=0].iloc[:,9:]
data 

# %%
data = other["#94"][occurance_per_row!=0].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(20,5))
_ = plt.hist2d(px, py, bins=[2048,30])

# %%
data = other["#67"][occurance_per_row!=0].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(20,5))
_ = plt.hist2d(px, py, bins=[2048,30])

# %%
data = other["#85"][occurance_per_row!=0].iloc[:,1032:1076]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(20,5))
_ = plt.hist2d(px, py, bins=[40,30])

# %%
data = other["#85"][occurance_per_row!=0].iloc[:,1200:1300]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(20,5))
_ = plt.hist2d(px, py, bins=[40,30])

# %%
other["#85"][occurance_per_row!=0][["datetime","sensor_type"]]

# %%
other["#85"][occurance_per_row!=0].iloc[0,1000:1300].plot()

# %%
other["#94"][occurance_per_row!=0][["datetime","sensor_type"]]

# %%
other["#67"][occurance_per_row!=0][["datetime","sensor_type"]]

# %% [markdown]
# # Values purely 127 

# %%
filtered2 = (other.df.iloc[:,9:]>=50) & ~filtered
outliers2 = other.df.iloc[:,9:][filtered2]

# %%
data = outliers2[occurance_per_row!=0].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(20,5))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[50,500]])

# %%
val3 = outliers2[occurance_per_row!=0].values

# %%
val3[~np.isnan(val3)]

# %%
fault_channels = outliers2.columns[outliers2[occurance_per_row!=0].sum()!=0].values

# %%
fault_channels_numbers = list(map(lambda x : int(x[7:]), fault_channels))

# %%
fcn = np.array(fault_channels_numbers)

# %%
for i,x in zip(fcn,np.diff(fcn)):
    if x!=1:
        print(i,x)

# %%
fcn

# %% [markdown]
# 1024:1054,1056: 1065,1194:1214,1216:1278

# %%
occurance_per_row = filtered2.sum(axis=1)

# %%
chart2_df = other.df.drop(other.df.iloc[:,9:].columns , axis=1)

# %%
chart2_df['occurances'] = occurance_per_row

# %%
axe = chart2_df.groupby(['datetime']).sum().plot(y='occurances',kind='bar')
axe.set_title('Time evolution of number of very high threshold occurances')
axe.set_xlabel('Calib date')
axe.set_ylabel('# of occurances')

# %%
chart3_df = chart2_df[chart2_df.occurances != 0]

# %%
x = chart3_df.datetime
labels = chart3_df['sensor_number'].unique()
todf = {k:[] for k in labels}
yx = []
for date in x:
    for label in labels:
        data_ = chart3_df[chart3_df.datetime==date]
        val = data_[data_.sensor_number==label].occurances.values
        if not val:
            val = 0
        else:
            val = val[0]
        todf[label].append(val)
occurances_df3 = pd.DataFrame(todf,index=x.values)
occurances_df3 = occurances_df3.sort_index()

# %%
occurances_df3 = occurances_df3.drop_duplicates()

# %%
run1 = occurances_df3[occurances_df3.index <= '2013-01-30']

# %%
run2 = occurances_df3[occurances_df3.index > '2013-01-30']

# %%
run1 = run1.drop(run1.columns[run1.sum()==0],axis=1)

# %%
fig, axe = plt.subplots(1,1,figsize=(20,10))
occurances_df3.plot(kind='bar', ax=axe,align="center", stacked=True, colormap='tab20')
plt.gca().xaxis.set_major_formatter(plt.FixedFormatter(occurances_df3.index.to_series().dt.strftime("%d-%m-%Y")))
fig.autofmt_xdate()

# %%
dataplot = occurances_df3.copy()


# %%
for sensor,column in occurances_df3.iteritems():
    dataplot[sensor][column!=0]=int(sensor[1:])
dataplot = dataplot.transpose()

# %%
fig, ax = plt.subplots(1,1,figsize=(15,6))
for sensor in dataplot:
    ax = dataplot[sensor].plot(linestyle='',marker='o', ax=ax, markersize=8)

# %%
dataplot.plot(y=dataplot.index,x=dataplot.columns,marker='.',linestyle='')

# %%
fig, axe = plt.subplots(1,1,figsize=(20,10))
run1.plot(kind='bar', ax=axe,align="center", stacked=True, colormap='tab20')
plt.gca().xaxis.set_major_formatter(plt.FixedFormatter(run1.index.to_series().dt.strftime("%d-%m-%Y")))
fig.autofmt_xdate()

# %%
run2

# %%
run2 = run2.drop(run2.columns[run2.sum()==0],axis=1)

# %%
fig, axe = plt.subplots(1,1,figsize=(20,10))
run2.plot(kind='bar', ax=axe,align="center", stacked=True, colormap='tab20')
plt.gca().xaxis.set_major_formatter(plt.FixedFormatter(run2.index.to_series().dt.strftime("%d-%m-%Y")))
fig.autofmt_xdate()

# %%
axe = chart2_df.groupby(['sensor_number']).sum().plot(y='occurances', kind='bar')
axe.set_title('Occurances of very high threshold per sensor')
axe.set_xlabel('Sensor')
axe.set_ylabel('# of occurances')

# %%
groupbysensor= chart2_df.groupby(['sensor_number','sensor_type']).sum()

# %%
axe = groupbysensor[groupbysensor.occurances!=0].plot(y='occurances',kind='bar')
axe.set_title('Occurances of very high threshold per sensor')
axe.set_xlabel('Sensor')
axe.set_ylabel('# of occurances')

# %%
groupbysensor[groupbysensor.occurances!=0]

# %% [markdown]
# # Other weird thing found with trending

# %%
occurance_per_row = filtered.sum(axis=1)

# %%
chart1_df = other.df.drop(other.df.iloc[:,9:].columns , axis=1)

# %%
chart1_df['occurances'] = occurance_per_row

# %%
axe = chart1_df.groupby(['datetime']).sum().plot(y='occurances',kind='bar')
axe.set_title('Time evolution of number of very high threshold occurances')
axe.set_xlabel('Calib date')
axe.set_ylabel('# of occurances')

# %%
axe = chart1_df.groupby(['sensor_number']).sum().plot(y='occurances', kind='bar')
axe.set_title('Occurances of very high threshold per sensor')
axe.set_xlabel('Sensor')
axe.set_ylabel('# of occurances')

# %%
data = other["#6"].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[5,30]])
axe.set_title('Sensor #85, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
other["#6"][(other["#6"].df.iloc[:,913:914]>=14.).sum(axis=1)!=0]

# %%
data = other["R"][other.df['datetime']=="2011-03-07"].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[5,30]])
axe.set_title('Sensor #85, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
data = other["phi"][other.df['datetime']=="2011-03-07"].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[5,30]])
axe.set_title('Sensor #85, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
data = other["R"][other.df['datetime']=="2012-08-02"].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(20,5))
_ = plt.hist2d(px, py, bins=[2048,25], range=[[0,2048],[5,30]])
axe.set_title('Sensor #85, pedestal distribution per channel')
axe.set_xlabel('Channel cvnumber')
axe.set_ylabel('ADC')

# %%
data = other["R"][other.df['datetime']=="2011-03-07"].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(20,5))
_ = plt.hist2d(px, py, bins=[2048,25], range=[[0,2048],[5,30]])
axe.set_title('Sensor #85, pedestal distribution per channel')
axe.set_xlabel('Channel cvnumber')
axe.set_ylabel('ADC')

# %%
data.mean().mean()

# %%
data = other["phi"][other.df['datetime']=="2012-08-02"].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,25], range=[[0,2048],[5,30]])
axe.set_title('Sensor #85, pedestal distribution per channel')
axe.set_xlabel('Channel cvnumber')
axe.set_ylabel('ADC')

# %%
(data==16.).sum().sum()

# %%
