# %%

# coding: utf-8

import numpy




# %%
import sys
sys.path.append('./calina')


from calibration_dataset import Tell1Dataset

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


import numpy as np


from calibration_dataset import Tell1Dataset
import calibration_dataset as DS

class MyDS(Tell1Dataset):
    filename_format = '%Y-%m-%d'
    filename_regex_format = r'\d{4}-\d{2}-\d{2}.csv'

datapath = "../../data/calibrations/"
data_list = MyDS.get_filepaths_from_dir(datapath)
dataset = MyDS(data_list, read=True)

bad_calibration = ["2011-03-07", "2012-08-02", "2012-07-30", "2012-08-01"]

header_list = [0, 32, 64, 96, 159, 191, 223, 255, 287, 319, 351, 383, 415, 447, 479, 511, 543, 575, 607, 639, 640, 672, 704, 736, 768, 800, 832, 864, 896, 928, 960, 992, 1024, 1056, 1088, 1120, 1183, 1215, 1247, 1279, 1311, 1343, 1375, 1407, 1439, 1471, 1503, 1535, 1567, 1599, 1631, 1663, 1664, 1696, 1728, 1760, 1792, 1824, 1856, 1888, 1920, 1952, 1984, 2016]
header_columns = ["channel{}".format(c) for c in header_list]

des = dataset.dfh['R'].df
des = des.drop(labels=header_columns,axis='columns')
des = des[((des.iloc[:,9:]>=50).sum(axis=1) == 0)]

badcals ={
     '2011-03-07':13.,
    '2012-08-02':15.,
    '2012-07-30': 22.,
    '2012-08-01':22.
}

for dt in des.datetime.unique():
    key = str(dt)[:10]
    val = badcals.get(key, 12.)
    des.loc[des.datetime==dt,'x'] = val

des.x.unique()

wrong = des[des['datetime'].isin(bad_calibration)]
other = des[~des['datetime'].isin(bad_calibration)]
wrong = DS.DatasetTree.from_df(wrong)
other = DS.DatasetTree.from_df(other)


X = des.x.copy()



del des['x']


traindata = other.df.iloc[:,10:510].values

train2data = wrong.df.iloc[:,10:510].values
xtrain2data = wrong.df.x.values-12


X.values[:,np.newaxis].shape

# %%
import pymc3 as pm
import theano.tensor as T

with pm.Model() as model:
    sds = pm.Uniform("sds", 0, 0.75, shape=traindata.shape[1])
    centers = pm.Uniform("centers", 0,50, shape=traindata.shape[1])
    base = pm.Uniform('err',-30,40)
    pr = pm.Uniform('errp',0,0.5)
    rder = pm.Bernoulli('errb',p=pr)
    error = base*rder
    signal = pm.Normal("obs", mu=centers, sd=sds, observed=traindata)*(1-rder)
    observations = signal+error


with model:
    step = pm.Metropolis(vars=[sds,centers, observations])
    trace = pm.sample(800, tune=200,step=step)


# %%
trace['centers'].mean(axis=0)

# %%
pm.backends.ndarray.save_trace(trace, 'tracedump')

# %%
