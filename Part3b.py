# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.7.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
import numpy

# %%
# %load_ext autoreload
# %autoreload 2 

# %%
import sys
sys.path.append('../src')

from dataset import Tell1Dataset, DatasetTree
import dataset as DS

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# %matplotlib inline
from ipywidgets import interact
import numpy as np

from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()
import matplotlib
matplotlib.rcParams['image.cmap'] = 'rainbow'

# %% [markdown]
# Read dataset:

# %%
dataset = Tell1Dataset(r'C:\repositories\Phd\LHCb\tell1_analysis\data\calibrations')


# %%
def sensor_histogram(data):
    x_data_list = []
    y_data_list = []
    for i,column in enumerate(data):
        y_data = list(data[column].values)
        y_data_list += y_data
        x_data_list += [i]*len(y_data)
    return x_data_list, y_data_list


# %% [markdown]
# ## R-type, ghosting

# %% [markdown]
# To determine whats causing this ghosting, lets just inspect entries with values greater than 15.0

# %%
dfhr = dataset.dfh['R'].df
increased = dfhr.iloc[:,9:]>15.0

# %% [markdown]
# Lets check the sum of entriex exceeding 15 per sensor calibration

# %%
high_values = dfhr[increased.sum(axis=1)>400]

# %% [markdown]
# And see at what dates did they occur

# %%
bad_calibration = high_values['datetime'].unique()
bad_calibration

# %% [markdown]
# Those are the dates that correspond with callibration process errors.

# %%
wrong = dataset.dfh.df[dataset.dfh.df['datetime'].isin(bad_calibration)]
other = dataset.dfh.df[~dataset.dfh.df['datetime'].isin(bad_calibration)]
wrong = DS.DatasetTree.from_df(wrong)
other = DS.DatasetTree.from_df(other)

# %% [markdown]
# # Wild things and vampires

# %%
filtered = (other.df.iloc[:,9:]>=50) & (other.df.iloc[:,9:]!=127.0)
outliers = other.df.iloc[:,9:][filtered]
occurance_per_row = filtered.sum(axis=1)
chart1_df = other.df.drop(other.df.iloc[:,9:].columns , axis=1)
chart1_df['occurances'] = occurance_per_row
chart2_df = chart1_df[chart1_df.occurances != 0]

# %%
x = chart2_df.datetime
labels = chart2_df['sensor_number'].unique()
todf = {k:[] for k in labels}
yx = []
for date in x:
    for label in labels:
        data_ = chart2_df[chart2_df.datetime==date]
        val = data_[data_.sensor_number==label].occurances.values
        if not val:
            val = 0
        else:
            val = val[0]
        todf[label].append(val)
occurances_df = pd.DataFrame(todf,index=x.values)
occurances_df = occurances_df.sort_index()

# %%
occurances_df

# %%
fig, axe = plt.subplots(1,1,figsize=(10,6))
occurances_df.plot(kind='bar', ax=axe,align="center", stacked=True)
plt.gca().xaxis.set_major_formatter(plt.FixedFormatter(occurances_df.index.to_series().dt.strftime("%d-%m-%Y")))
fig.autofmt_xdate()
axe.set_title(' Number of callibrations  (value>=50 and value != 127) for particular sensors, in time')
axe.set_xlabel('Date of callibration')
axe.set_ylabel('Number of occurances')

# %%
persensor = chart1_df.groupby(['sensor_number']).sum()

# %%
persensor[persensor['occurances']!=0]

# %% [markdown]
# all are phi-type

# %%
data = other[occurance_per_row!=0].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[50,500]],cmin=1)
axe.set_title('Phi-type sensor callibrations where value >=50 and value != 127')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
data = other["#67"][occurance_per_row!=0].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30],cmin=1)
axe.set_title('Callibrations of #67 with values >=50 and value != 127')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
data = other["#85"][occurance_per_row!=0].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30],cmin=1)
axe.set_title('Callibrations of #85 with values >=50 and value != 127')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
data = other["#94"][occurance_per_row!=0].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30],cmin=1)
axe.set_title('Callibrations of #94 with values >=50 and value != 127')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% [markdown]
# # Values purely 127 

# %%
filtered2 = (other.df.iloc[:,9:]>=50) & ~filtered
outliers2 = other['R'].df.iloc[:,9:][filtered2]
outliers2_phi = other['phi'].df.iloc[:,9:][filtered2]

# %%
data = outliers2
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[50,500]],cmin=1)
axe.set_title('Occurances of values 127 in R type sensors')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
data = outliers2_phi
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[50,500]],cmin=1)
axe.set_title('Occurances of values 127 in Phi type sensors')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
outliers2.sum().plot(figsize=(20,10),color='blue', alpha=0.5)
outliers2_phi.sum().plot(figsize=(20,10),color='red', alpha=0.5)

# %%
val3 = outliers2[occurance_per_row!=0].values
np.unique(val3[~np.isnan(val3)])

# %%
fault_channels = outliers2.columns[outliers2[occurance_per_row!=0].sum()!=0].values
fault_channels_numbers = list(map(lambda x : int(x[7:]), fault_channels))
fcn = np.array(fault_channels_numbers)
fcn

# %% [markdown]
# 1024:1054,1056: 1065,1194:1214,1216:1278

# %%
occurance_per_row = filtered2.sum(axis=1)
chart3_df = other.df.drop(other.df.iloc[:,9:].columns , axis=1)
chart3_df['occurances'] = occurance_per_row

# %%
axe = chart3_df.groupby(['datetime']).sum().plot(y='occurances',kind='bar')
axe.set_title('Time evolution of number of very high threshold occurances')
axe.set_xlabel('Calib date')
axe.set_ylabel('# of occurances')

# %%
chart_tested = chart3_df[chart3_df.occurances != 0]

# %%

x = chart_tested.datetime
labels = chart_tested['sensor_number'].unique()
todf = {k:[] for k in labels}
yx = []
for date in x:
    for label in labels:
        data_ = chart_tested[chart_tested.datetime==date]
        val = data_[data_.sensor_number==label].occurances.values
        if not val:
            val = 0
        else:
            val = val[0]
        todf[label].append(val)
occurances_df3 = pd.DataFrame(todf,index=x.values)
occurances_df3 = occurances_df3.sort_index()
occurances_df3 = occurances_df3.drop_duplicates()

# %%
run1 = occurances_df3[occurances_df3.index <= '2013-01-30']

# %%
run2 = occurances_df3[occurances_df3.index > '2013-01-30']

# %%
run1 = run1.drop(run1.columns[run1.sum()==0],axis=1)

# %%
fig, axe = plt.subplots(1,1,figsize=(16,8))
occruances_df_plot1 = occurances_df3.copy()
occruances_df_plot1["#32+#29"] = occruances_df_plot1["#32"]+occruances_df_plot1["#29"]
occruances_df_plot1["#33+#19"] = occruances_df_plot1["#33"]+occruances_df_plot1["#19"]
occruances_df_plot1["#38+#5"] = occruances_df_plot1["#38"]+occruances_df_plot1["#5"]
occruances_df_plot1["#87+#94"] = occruances_df_plot1["#87"]+occruances_df_plot1["#94"]
occruances_df_plot1["#6+#27"] = occruances_df_plot1["#6"]+occruances_df_plot1["#27"]
del occruances_df_plot1["#32"]
del occruances_df_plot1["#29"]
del occruances_df_plot1["#19"]
del occruances_df_plot1["#33"]
del occruances_df_plot1["#38"]
del occruances_df_plot1["#5"]
del occruances_df_plot1["#87"]
del occruances_df_plot1["#94"]
del occruances_df_plot1["#6"]
del occruances_df_plot1["#27"]
occruances_df_plot1.plot(kind='bar', ax=axe,align="center", stacked=True, colormap='tab20')
plt.gca().xaxis.set_major_formatter(plt.FixedFormatter(occurances_df3.index.to_series().dt.strftime("%d-%m-%Y")))
fig.autofmt_xdate()
axe.set_title('Occurances of values 127 in sensors', fontsize=20)
axe.set_xlabel('Dates of callibration', fontsize=15)
axe.set_ylabel('Number of occurances', fontsize=15)

# %%
dataplot = occurances_df3.copy()


# %%
for sensor,column in occurances_df3.iteritems():
    dataplot[sensor][column!=0]=int(sensor[1:])
dataplot = dataplot.transpose()

# %%
neworder = list(map(lambda x: "#"+str(x),sorted(int(col[1:]) for col in occurances_df3.columns)))

# %%
occurances_df4 = occurances_df3.reindex_axis(neworder, axis=1)
occurances_df4

# %%
import matplotlib.dates as mdates
fig, axe = plt.subplots(1,1,figsize=(20,10))

for sensor, column in occurances_df4.iteritems():
    print(column)
    prev = 0 
    end = 0
    value = 0
    for date, value in column.iteritems():
        if value != prev and prev != 0:
            print("here")
            axe.barh(sensor,  end-start, left=start)
        else:
            start = mdates.date2num(date)
        end = mdates.date2num(date)
        prev = value
        print(date, "|", value, prev, "|" ,end, start)
    break
    #axe.barh("#36",  40, left=1995)


# %%
y_l

# %%
len(enddates)

# %%
mdates.num2date(735000)

# %%
y_labels

# %%
import matplotlib.dates as mdates
from matplotlib.colors import LogNorm
from matplotlib.colorbar import ColorbarBase
norm = LogNorm(vmin=1,vmax=128)
fig, axe = plt.subplots(1,2,figsize=(12,6), gridspec_kw = {'width_ratios':[20, 1]})
y_labels = occurances_df4.columns.tolist()
sensor_to_y = dict(zip(y_labels,range(len(y_labels))))
startdates = []
enddates = []
intervals = []
values = []
y_l = []
for sensor, column in occurances_df4.iteritems():
    prev = 0 
    end = 0
    value = 0
    recordval = 0
    size = len(column)
    for i, (date, value) in enumerate(column.iteritems()):
        datepy = date.to_pydatetime()
        if prev!=value and recordval==0:
            start = datepy
            recordval = value
            values.append(value)
            startdates.append(datepy)
            #print("start",datepy,"val",value)
        elif prev!=value:
            end=datepy
            enddates.append(end)
            y_l.append(sensor)
            #print("end",datepy,"val",value)
            if value!=0:
                start = datepy
                values.append(value)
                startdates.append(datepy)
                #print("start",datepy,"val",value)
            recordval=value
        if value==recordval and (i==size-1 and recordval!=0):
            values.append(value)
            end=datepy
            enddates.append(end)
            y_l.append(sensor)
            #print("end",datepy,"val",value)
            recordval=0
        prev = value
maxval = occurances_df4.max().max()
cmap = plt.get_cmap("rainbow")
color_values = [cmap(norm(v)) for v in values]
edate, bdate = [mdates.date2num(item) for item in (enddates, startdates)]
yl = ["#"+str(100+y_labels.index(u)) for u in y_l]
axe[0].barh(yl, edate-bdate , left=bdate, color = color_values)    
axe[0].set_yticklabels(y_labels)
axe[0].xaxis_date()
axe[0].grid()
axe[0].set_title('Number of blocked channels (colour) for each sensor in time',fontsize=20)
axe[0].set_xlabel('Date of callibration',fontsize=15)
axe[0].set_ylabel('Sensor number',fontsize=15)
undates = np.unique(np.array(edate+bdate))
cb2 = ColorbarBase(axe[1], cmap=cmap,norm=norm,orientation='vertical')
axe[1].set_ylabel('Number of blocked channels (logscale)',fontsize=15)
axe[1].yaxis.set_label_position("right")
fig.tight_layout()
fig.autofmt_xdate()

# %%
