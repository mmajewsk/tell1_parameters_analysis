import numpy as np
import os
import pandas as pd


def split_to_variables(df):
	df_pedestals = df[df['type'] == 'pedestal']
	dfp = df_pedestals.dropna(axis=1, how='all')
	dfl = df[df['type'] == 'low_threshold']
	dfh = df[df['type'] == 'hit_threshold']
	return dfp, dfl, dfh


list_of_modules = np.array(list(range(38))+ [39,  40,  41,  48])
list_of_modules = ["mod_{}".format(i) for i in list_of_modules]
def create_slot_labels():
	labels = []
	for part in range(1,26):
		for i in "LR":
			labels.append("VL{:02d}{}".format(part,i))
	return labels
slot_labels = np.array(['PU01L', 'PU01R', 'PU02L', 'PU02R']+create_slot_labels(),dtype=object)
sensor_numbers= [ '#{}'.format(i) for i in np.array(list(range(42))+list(range(64,106))+list(range(128,132)))]

def find_dummy_channels(dataset : pd.DataFrame):
	blocked = (dataset==127.0)
	trully_dummy = []
	for channel in blocked:
		column = blocked[channel]
		if column.all():
			trully_dummy.append(channel)
	return trully_dummy

def remove_dummy_channels(data : pd.DataFrame):
	dummy_channels = find_dummy_channels(data.iloc[:, 9:])
	channels = data.drop(dummy_channels, axis=1)
	#assert channels.iloc[:,9:].shape[1] == 2048, "Number of channelsis not equal to 2048, found {}".format(channels.iloc[:,9:].shape[1])
	columns = ["channel{}".format(i) for i in range(channels.iloc[:,9:].shape[1])]
	channels.columns = channels.columns[:9].tolist()+columns
	return channels


class DatasetTree:
	tree = {
		'type':
			[
				'pedestal',
				'low_threshold',
				'hit_threshold',
			],
		'sensor': {
			'mod_nr': list_of_modules,
			'slot_label': slot_labels,
			'mod_type': ['PU', 'VELO_phi', 'VELO_R', 'VELO_Rx', 'VELO_phix'],
			'sensor_type': ['R', 'phi'],
			'sensor_number': sensor_numbers,
		}

	}
	module_map = pd.read_csv('../data/others/module_mapping.csv', sep=' ')
	def  __init__(self, df, tree, module_map):
		self.df = df.copy()
		self.tree = tree
		self.module_map = module_map.copy()

	@staticmethod
	def from_df(df):
		return DatasetTree(df, DatasetTree.tree, DatasetTree.module_map)

	@staticmethod
	def from_tell1dataset_df(df):
		res = df.merge(DatasetTree.module_map, left_on='sensor', right_on='sensor_number', how='outer')
		res = res[~res['sensor_number'].isnull()]
		res.loc[:, 'sensor_number'] = res['sensor_number'].apply(lambda x: '#' + str(int(x)))
		cols = res.columns.tolist()
		cols = cols[:3]+cols[-6:]+cols[3:-6]
		df = res[cols]
		return DatasetTree.from_df(df)


	def __match_type(self, item):
		type_list = [(item in v) for v in self.tree['type']]
		matches = sum(type_list)
		if matches==1:
			type_ind = type_list.index()
			type = self.tree['type'][type_ind]
			return DatasetTree(self.df[self.df['type']==type], self.tree, self.module_map)
		else:
			return None

	def __match_sensor(self, item):
		matches = []
		for k, v in self.tree['sensor'].items():
			if item in v:
				matches.append((k,item))
		if len(matches)==1:
			k,v = matches[0]
			return DatasetTree(self.df[self.df[k]==v], self.tree, self.module_map)

	def __getitem__(self, item):
		if isinstance(item,str):
			match = self.__match_type(item)
			if match is not None:
				return match
			match = self.__match_sensor(item)
			if match is not None:
				return match
		return self.df[item]

	def __repr__(self):
		return self.df.__repr__()




class Tell1Dataset:

	def __init__(self, data_dir=r'C:\repositories\Phd\LHCb\tell1_analysis\data'):
		self.data_dir = data_dir
		self._df = None
		self._dfp = None
		self._dfl = None
		self._dfh = None

	@staticmethod
	def remove_dummy_threshold_channels(df):
		df_r = remove_dummy_channels(df[df['sensor_type'] == 'R'])
		df_phi = remove_dummy_channels(df[df['sensor_type'] == 'phi'])
		df = pd.merge(df_r, df_phi, left_index=True, right_index=True)
		return df

	def check_if_splitted(self):
		if self._dfp is None or self._dfl is None or self._dfh is None:
			varaibles = split_to_variables(self.df.df)
			self._dfp, self._dfl, self._dfh = map(lambda x: DatasetTree(x, self.df.tree, self.df.module_map), varaibles)
			self.dfhr = remove_dummy_channels(self._dfh['R'].df)
			self.dfhphi = remove_dummy_channels(self._dfh['phi'].df)
			dfh = pd.concat([self.dfhr, self.dfhphi])
			self._dfh.df = dfh
			self.dflr = remove_dummy_channels(self._dfl['R'].df)
			self.dflphi = remove_dummy_channels(self._dfl['phi'].df)
			dfl = pd.concat([self.dflr, self.dflphi])
			self._dfl.df = dfl

	def remove_dummy(self, _df):
		df = _df.df
		self.tomerge = []
		for type in DatasetTree.tree['type']:
			parameter = df[df['type'] == type]
			parameter = Tell1Dataset.remove_dummy_threshold_channels(parameter)
			self.tomerge.append(parameter.copy())
		_df.df = df
		return _df

	@property
	def df(self):
		if self._df is None:
			self._df = self.join_data()
			self._df = DatasetTree.from_tell1dataset_df(self._df)
			self._df = self.remove_dummy(self._df)
		return self._df

	@property
	def dfp(self):
		self.check_if_splitted()
		return self._dfp

	@property
	def dfl(self):
		self.check_if_splitted()
		return self._dfl

	@property
	def dfh(self):
		self.check_if_splitted()
		return self._dfh


	def join_data(self):
		csv_list = os.listdir(self.data_dir)
		columns = ['type', 'sensor'] + ['channel{}'.format(i) for i in range(2304)]
		df_list = []
		for csv in csv_list:
			csv_path = os.path.join(self.data_dir,  csv)
			df = pd.read_csv(csv_path,sep=' ',names=columns)
			time, _ = os.path.splitext(csv)
			df['sensor'] = df['sensor'].apply(lambda x: int(x.split('VeloTELL1Board')[1]))
			df.insert(1, 'time', time)
			df_list.append(df.copy())
		df = pd.concat(df_list)
		df = df.sort_values(['time','sensor'],ascending=[1,1])
		df = df.reset_index(drop=True)
		df['datetime'] = pd.to_datetime(df['time'])
		columns = columns[:2] + ['datetime'] + columns[2:]
		df = df[columns]
		return df