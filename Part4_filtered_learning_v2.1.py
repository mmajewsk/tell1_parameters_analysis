# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.10.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
import numpy

# %%
# %load_ext autoreload
# %autoreload 2 

# %%
import sys
sys.path.append('calina/')

from calibration_dataset import Tell1Dataset, DatasetTree
import calibration_dataset as DS

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# %matplotlib inline
from ipywidgets import interact
import numpy as np


# %% [markdown]
# Read dataset:

# %%

from calibration_dataset import Tell1Dataset

class MyDS(Tell1Dataset):
    filename_format = '%Y-%m-%d'
    filename_regex_format = r'\d{4}-\d{2}-\d{2}.csv'

datapath = "../../data/calibrations/"
data_list = MyDS.get_filepaths_from_dir(datapath)
dataset = MyDS(data_list, read=True)


# %%
def sensor_histogram(data):
    x_data_list = []
    y_data_list = []
    for i,column in enumerate(data):
        y_data = list(data[column].values)
        y_data_list += y_data
        x_data_list += [i]*len(y_data)
    return x_data_list, y_data_list


# %%
bad_calibration = ["2011-03-07", "2012-08-02", "2012-07-30", "2012-08-01"]

# %%
header_list = [0, 32, 64, 96, 159, 191, 223, 255, 287, 319, 351, 383, 415, 447, 479, 511, 543, 575, 607, 639, 640, 672, 704, 736, 768, 800, 832, 864, 896, 928, 960, 992, 1024, 1056, 1088, 1120, 1183, 1215, 1247, 1279, 1311, 1343, 1375, 1407, 1439, 1471, 1503, 1535, 1567, 1599, 1631, 1663, 1664, 1696, 1728, 1760, 1792, 1824, 1856, 1888, 1920, 1952, 1984, 2016]
header_columns = ["channel{}".format(c) for c in header_list]

# %%
des = dataset.dfh['R'].df
des = des.drop(labels=header_columns,axis='columns')
des = des[((des.iloc[:,9:]>=50).sum(axis=1) == 0)]

# %%
badcals ={
     '2011-03-07':13.,
    '2012-08-02':15.,
    '2012-07-30': 22.,
    '2012-08-01':22.
}

# %%
for dt in des.datetime.unique():
    key = str(dt)[:10]
    val = badcals.get(key, 12.)
    des.loc[des.datetime==dt,'x'] = val

# %%
des.x.unique()

# %%
wrong = des[des['datetime'].isin(bad_calibration)]
other = des[~des['datetime'].isin(bad_calibration)]
wrong = DS.DatasetTree.from_df(wrong)
other = DS.DatasetTree.from_df(other)

# %%
X = des.x.copy()

# %%
del des['x']

# %%
traindata = other.df.iloc[:,10:510].values

# %%
train2data = wrong.df.iloc[:,10:510].values
xtrain2data = wrong.df.x.values-12

# %%
X.values[:,np.newaxis].shape

# %%
import pymc3 as pm
import theano.tensor as T

# %%


with pm.Model() as model:
    sds = pm.Uniform("sds", 0, 0.75, shape=traindata.shape[1])
    centers = pm.Uniform("centers", 0,50, shape=traindata.shape[1])
    base = pm.Uniform('err',-30,40)
    pr = pm.Uniform('errp',0,0.5)
    rder = pm.Bernoulli('errb',p=pr)
    error = base*rder
    signal = pm.Normal("obs", mu=centers, sd=sds, observed=traindata)*(1-rder)
    observations = signal+error

# %%
trace = pm.backends.ndarray.load_trace('tracedump', model)

# %%
az.plot_trace(trace)

# %%
import arviz as az

# %%
meancenters = trace['centers'].mean(axis=0)
meansds = trace['sds'].mean(axis=0)

# %%
with model:
    post_pred = pm.sample_ppc(trace, samples=4100)

# %%
data = pd.DataFrame(post_pred['obs'])
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[100,30], range=[[0,100],[0,30]],cmin=1)
axe.set_title('R-type module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
data = pd.DataFrame(traindata)
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[100,30], range=[[0,100],[0,30]],cmin=1)
axe.set_title('R-type module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
import pymc3 as pm
import theano.tensor as T

with pm.Model() as model:
    x = pm.Uniform('x', 0,40, observed=xtrain2data[:,np.newaxis])
    k = pm.Uniform('k', -10,10,shape=traindata.shape[1])
    m = pm.Uniform('m', -10,10,shape=traindata.shape[1])
    observations = pm.Normal("obs", mu=meancenters+k*x, sd=meansds+m*x, observed=train2data)
    #observations2= pm.Uniform('obs2', lower=pm.math.floor, upper=pm.math.ceil
    #observations = pm.Deterministic('toint', )

# %%
with model:
    #step1 = pm.Metropolis(vars=[p, sds, centers])c
    #step2 = pm.ElemwiseCategorical(vars=[assignment])
    #trace = pm.sample(25000, step=[step1, step2])
    step = pm.Metropolis(vars=[k,m, observations])
    trace = pm.sample(8000, tune=2000,step=step)

# %%
pm.traceplot(trace)

# %%
with model:
    post_pred2 = pm.sample_ppc(trace, samples=2000)

# %%
postdata = np.mean(post_pred2['obs'], axis=0)

# %%
plt.plot(np.mean(trace['x'],axis=0))
plt.plot(np.mean(trace['k'],axis=0))

# %%
data = pd.DataFrame(postdata)
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[100,30], range=[[0,100],[0,30]],cmin=1)
axe.set_title('R-type module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
simval = np.mean(post_pred['obs'],axis=0)

# %%
data = pd.DataFrame(train2data)
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[100,30],cmin=1)
axe.set_title('R-type module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
bad_calibration

# %%
data = wrong.df[wrong.df.datetime=='2012-08-02'].iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]],cmin=1)
axe.set_title('R-type module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
kmeans = np.mean(trace['k'],axis=0)
mmeans = np.mean(trace['m'],axis=0)

# %%
model

# %%
import pymc3 as pm
import theano.tensor as T

with pm.Model() as model:
    x = 19.
    k = kmeans
    m = mmeans
    observations = pm.Normal("obs", mu=meancenters+k*x, sd=meansds+m*x, shape=100)

# %%
with model:
    step = pm.Metropolis(vars=[observations])
    trace3 = pm.sample(8000, tune=2000,step=step)

# %%
trace3['obs'].shape

# %%
data = pd.DataFrame(trace3['obs'])
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[100,30],cmin=1)#, range=[[0,100],[0,30]],cmin=1)
axe.set_title('R-type module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
