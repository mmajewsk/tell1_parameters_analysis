# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.11.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% pycharm={"is_executing": false}
import numpy

# %% pycharm={"is_executing": false}
# %load_ext autoreload
# %autoreload 2 

# %% pycharm={"is_executing": false}
import sys
sys.path.append('./calina')

from calibration_dataset import Tell1Dataset, DatasetTree
import calibration_dataset as DS

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# %matplotlib inline
from ipywidgets import interact
import numpy as np

from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()
import matplotlib
matplotlib.rcParams['image.cmap'] = 'rainbow'

# %% [markdown]
# Read dataset:

# %% pycharm={"is_executing": false}
dataset_path = '/home/mwm/repositories/LHCb/ml_analysis_recreation/data/calibrations'
class MyDS(Tell1Dataset):
    filename_format = '%Y-%m-%d'
    filename_regex_format = r'\d{4}-\d{2}-\d{2}.csv'
data_list = MyDS.get_filepaths_from_dir(dataset_path)
dataset = MyDS(data_list, read=True)
from pathlib import Path
plot_output = Path('/home/mwm/repositories/LHCb/ml_analysis_recreation/paper/pics/')


# %% pycharm={"is_executing": false}
def sensor_histogram(data):
    x_data_list = []
    y_data_list = []
    for i,column in enumerate(data):
        y_data = list(data[column].values)
        y_data_list += y_data
        x_data_list += [i]*len(y_data)
    return x_data_list, y_data_list


# %% pycharm={"is_executing": false}

# %% [markdown]
# # Thresholds

# %% [markdown]
# Thresholds are used to determine the hit in detector. They are calculated by measuring the standard deviation of signal in channel. High threshold is 6 sigma and low i s 2 sigma (or smth), so 

# %% [markdown]
# The plot below represents the high threshold value in **all calibrations** for **all sensors**, both R-type and Phi-type.

# %% pycharm={"is_executing": false}
data = dataset.dfh.df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], cmin=1)
axe.set_title('All module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% [markdown]
# There are some values that are bumping the scope of the histogram. lets zoom to the bottom part

# %% pycharm={"is_executing": false}
data = dataset.dfh.df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2148],[0,30]], cmin=1)
axe.set_title('All module data, high threshold distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% pycharm={"is_executing": false, "name": "#%%\n"}
weird = (data > 25).sum(axis=1)>10

# %% pycharm={"is_executing": true, "name": "#%%\n"}
weird_hits = dataset.dfh.df.loc[weird.index[weird==True],:]

# %%
dfw = dataset.dfh.df.copy()

# %%
dfw.iloc[:,9:] = (data>25)

# %%
dfw.groupby(["datetime"]).sum().iloc[:,2:].sum(axis=0)

# %%
dfw.groupby(["sensor"]).sum().iloc[:,2:].sum(axis=1)

# %% [markdown]
# we can see that adc oscillates somewhat around value of 10-12. Those data are to general, we need to separate R type sensor and phi type

# %% [markdown]
# ### R-type

# %% pycharm={"is_executing": false}
data = dataset.dfh['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30],cmin=1)
axe.set_title('R-type module data, high threshold distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% [markdown]
# ### Phi - type

# %% pycharm={"is_executing": false}
data = dataset.dfh['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30],cmin=1)
axe.set_title('Phi-type module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% pycharm={"is_executing": false}
data = dataset.dfh['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(2,1,figsize=(15,7))
_, xedges, yedges, _  = axe[0].hist2d(px, py, bins=[2048,30],cmin=1)
data = dataset.dfh['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
_, xedges, yedges, _   = axe[1].hist2d(px, py,range=[[xedges[0],xedges[-1]],[yedges[0],yedges[-1]]], bins=[2048,30],cmin=1)
axe[1].grid(linestyle='dashed')
axe[1].set_axisbelow(True)
axe[0].grid(linestyle='dashed')
axe[0].set_axisbelow(True)
axe[0].set_title('Phi-type module data, high threshold distribution per channel')
axe[0].set_xlabel('Channel number')
axe[0].set_ylabel('ADC')
axe[1].set_title('R-type module data, high threshold distribution per channel')
axe[1].set_xlabel('Channel number')
axe[1].set_ylabel('ADC')

fig.tight_layout()
fig.savefig(plot_output/"P2-threshold-all-r-phi.png")
fig.show()

# %% [markdown]
# In both cases we can see that values oscillate around 10, but faint 'ghost-distribution' is visible.

# %% [markdown]
# ## R-type, ghosting

# %% [markdown]
# To determine whats causing this ghosting, lets just inspect entries with values greater than 15.0

# %% pycharm={"is_executing": false}
dfhr = dataset.dfh['R'].df
increased = dfhr.iloc[:,9:]>15.0

# %% [markdown]
# Lets check the sum of entriex exceeding 15 per sensor calibration

# %% pycharm={"is_executing": false}
fig, axe = plt.subplots(1,1)
_ = increased.sum(axis=1).hist(bins=20, figsize=(8,6))
axe.set_title('Number of occurances of values higher than 15 in high thresholds of R-type sensors, per sensor')
axe.set_xlabel('Number of sensors')
axe.set_ylabel('Number of occurances')

# %% pycharm={"is_executing": false}
fig, axe = plt.subplots(1,1)
increased.sum(axis=1).hist(bins=20, log=True, figsize=(8,6))
axe.set_title('Number of occurances of values higher than 15 in high thresholds of R-type sensors, per sensor')
axe.set_xlabel('Number of sensors')
axe.set_ylabel('Number of occurances')
fig.savefig(plot_output/"P2-higher15-hist.png")

# %% [markdown]
# We can see that there are outliers with more than 400 entries that exceed that value, lets separate those entries.

# %% pycharm={"is_executing": false}
high_values = dfhr[increased.sum(axis=1)>400]

# %% [markdown]
# And see at what dates did they occur

# %% pycharm={"is_executing": false}
bad_calibration = high_values['datetime'].unique()
bad_calibration

# %% pycharm={"is_executing": false}

# %% pycharm={"is_executing": false}
data = dataset.dfh['phi']['#67'].df
data = data.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30],cmin=1)
axe.set_title('Phi-type module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% [markdown]
# Those are the dates that correspond with callibration process errors.

# %% [markdown]
# lets look at the distributions per channel again, after excluding those dates

# %% pycharm={"is_executing": false}
wrong = dataset.dfh.df[dataset.dfh.df['datetime'].isin(bad_calibration)]
other = dataset.dfh.df[~dataset.dfh.df['datetime'].isin(bad_calibration)]
wrong = DS.DatasetTree.from_df(wrong)
other = DS.DatasetTree.from_df(other)

# %% [markdown]
# #### R-type sensors for High threshold

# %% [markdown]
# Wrong

# %% pycharm={"is_executing": false}
data = wrong['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]],cmin=1)
axe.set_title('Bad callibration histogram, R-type module data, high threshold distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% [markdown]
# Good

# %% pycharm={"is_executing": false}
data = other['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,25], range=[[0,2048],[5,30]],cmin=1)
axe.set_title('Filtered histogram, R-type module data, high threshold distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% pycharm={"is_executing": false}
fig, axe = plt.subplots(1,1, figsize=(8,6))
_ = plt.hist(other['R'].df.iloc[:,9:].values.ravel(),bins=50, log=True)
axe.set_title('Histogram of filtered high threshild values for R-type sensors')
axe.set_xlabel('ADC')

# %%
data.mean().mean()

# %% [markdown]
# #### Phi-type for High threshold:

# %% [markdown]
# worng

# %%
data = wrong['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]],cmin=1)
axe.set_title('Bad callibration histogram, Phi-type module data, high threshold distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% [markdown]
# good

# %%
data = other['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30],  range=[[0,2048],[0,30]],cmin=1)
axe.set_title('Filtered histogram, Phi-type module data, high threshold distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
fig, axe = plt.subplots(1,1, figsize=(8,6))
_ = plt.hist(other['phi'].df.iloc[:,9:].values.ravel(),bins=50, log=True)
axe.set_title('Histogram of filtered high threshild values for Phi-type sensors')
axe.set_xlabel('ADC')

# %% [markdown]
# ## Important !!
#  - We see some faint peculiar distribution in some channels, in both good calibrations and bad
#  - still, there are some high values, that are outside the scope of the plotsabove 

# %% [markdown]
# # Header cross-talk

# %% [markdown]
# those faint peculiar distributions are related to header cross talk, to take a better look at them we need to look at specific channels in modules

# %%
from sklearn.neighbors.kde import KernelDensity

# %%
plotset = other
y = plotset['R']['#0']['channel0'].values.T[:, np.newaxis]
x = np.linspace(5, 30, 500)[:, np.newaxis]
kde = KernelDensity(kernel='gaussian', bandwidth=0.7).fit(y)
log_dens = kde.score_samples(x)
y = np.exp(log_dens)
p = figure(title="simple line example", plot_height=300, plot_width=600)
r = p.line(x[:,0], y, color="#2222aa", line_width=3)
def update(channel, bandwidth):
    y = plotset['R']['channel'+str(channel)].values.T[:, np.newaxis]
    kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(y)
    log_dens = kde.score_samples(x)
    y = np.exp(log_dens)
    r.data_source.data['y'] = y
    push_notebook()
show(p, notebook_handle=True)

# %%
interact(update, channel=(0,2047), bandwidth=(0.,1.,0.05))

# %% [markdown]
# 0, 32, 64, 96, 159, 191, 223, 255, 287, 319, 351, 383, 415, 447, 479, 511, 543, 575, 607, 639, 640!, 672, 704, 736, 768, 800, 832, 864, 896, 928, 960, 992, 1024, 1056, 1088, 1120, 1183, 1215, 1247, 1279, 1311, 1343, 1375, 1407, 1439, 1471?, 1503, 1535, 1567, 1599, 1631, 1663, 1664?, 1696, 1728, 1760, 1792, 1824, 1856, 1888, 1920, 1952, 1984, 2016

# %% [markdown]
# We used kernel density estimators to eleiminate binning problem. As we can see in interactive plot above, we cane separate the cahnnels that are experiencing header cross talk by calculating KDE peaks, with manually selected bandwith.

# %% [markdown]
# Below is small snippet allowing for channel per sensor analysis of distribution.

# %%
plotset = other
y = plotset['#0']['channel0'].values.T[:, np.newaxis]
x = np.linspace(5, 30, 500)[:, np.newaxis]
kde = KernelDensity(kernel='gaussian', bandwidth=0.7).fit(y)
log_dens = kde.score_samples(x)
y = np.exp(log_dens)
p = figure(title="simple line example", plot_height=300, plot_width=600)
r = p.line(x[:,0], y, color="#2222aa", line_width=3)
def update(sensor, channel, bandwidth):
    y = plotset[sensor]['channel'+str(channel)].values.T[:, np.newaxis]
    kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(y)
    log_dens = kde.score_samples(x)
    y = np.exp(log_dens)
    r.data_source.data['y'] = y
    push_notebook()
show(p, notebook_handle=True)

# %%
interact(update, sensor=DS.sensor_numbers, channel=(0,2047), bandwidth=(0.,1.,0.05))

# %%
plotset = other
y = plotset['#0']['channel0'].values.T[:, np.newaxis]
x = np.linspace(5, 30, 500)[:, np.newaxis]
kde = KernelDensity(kernel='gaussian', bandwidth=0.7).fit(y)
log_dens = kde.score_samples(x)
y = np.exp(log_dens)
p = figure(title="simple line example", plot_height=300, plot_width=600)
r = p.line(x[:,0], y, color="#2222aa", line_width=3)
def update(sensor, channel, bandwidth):
    y = plotset[sensor]['channel'+str(channel)].values.T[:, np.newaxis]
    kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(y)
    log_dens = kde.score_samples(x)
    y = np.exp(log_dens)
    r.data_source.data['y'] = y
    push_notebook()
show(p, notebook_handle=True)

# %%
plotset = other['R'].df.iloc[:,9:]


# %%
def calculate_column_peak(df):
    y = df.values.T[:, np.newaxis]
    x = np.linspace(5, 30, 500)[:, np.newaxis]
    kde = KernelDensity(kernel='gaussian', bandwidth=0.45).fit(y)
    log_dens = kde.score_samples(x)
    y = np.exp(log_dens)
    return x[np.argmax(y)][0]


# %%
peakz = plotset.aggregate(calculate_column_peak)

# %%
plt.plot(peakz.values)


# %% [markdown]
#

# %% [markdown]
# # Trending of thresholds

# %%
from sklearn import linear_model
plotset = other['R']["#0"].df
y = plotset['channel0']
x = plotset['datetime']
model = linear_model.LinearRegression()
modelx = x.values[:,np.newaxis]
modely = y.values[:,np.newaxis]
model.fit(modelx.astype(np.float64), modely)
Y_pred = model.predict(modelx.astype(np.float64))
p = figure(title="Individual channel pedestal values of sensor in time, with linear trend", plot_height=300, plot_width=600)
r = p.circle(x, y, color="#2222aa", line_width=3)
r2 = p.line(x, Y_pred[:,0], color="green", line_width=3)
from bokeh.models import DatetimeTickFormatter
p.xaxis.formatter=DatetimeTickFormatter(
        hours=["%d %B %Y"],
        days=["%d %B %Y"],
        months=["%d %B %Y"],
        years=["%d %B %Y"],
    )
p.xaxis.major_label_orientation = 3.1415/4
def update2(sensor, channel):
    model = linear_model.LinearRegression()
    modelx = x.values[:,np.newaxis]
    plotset = other['R'][sensor].df
    y = plotset['channel'+str(channel)]
    modely = y.values[:,np.newaxis]
    model.fit(modelx.astype(np.float64), modely)
    Y_pred = model.predict(modelx.astype(np.float64))
    r.data_source.data['y'] = y
    r2.data_source.data['y'] = Y_pred[:,0]
    push_notebook()
show(p, notebook_handle=True)

# %%
interact(update2, sensor=other["R"].df.sensor_number.unique(), channel=(0,2047))

# %%
from sklearn.preprocessing import normalize

def get_coef(x,y):
    model = linear_model.LinearRegression()
    modely = y.values[:,np.newaxis]
    modelx = x[:,np.newaxis]
    model.fit(modelx, modely)
    return model.coef_[0]

def get_df_coef(df):
    time = df['datetime'].values
    time = (time-time.min())/np.timedelta64(1,'D')
    ret = []
    for column in df.iloc[:,9:]:
        y=df[column]
        coef = get_coef(time, y)
        ret.append(coef)
    return np.array(ret).T


# %%
group = other.df.groupby(['sensor_number'])
results = {}
for groupname, data in group:
    results[groupname] = get_df_coef(data)[0]

# %%
coefs = pd.DataFrame.from_dict(results,orient='index')
coefs.columns = [ "channel"+str(i) for i in list(coefs.columns)]
coefs = coefs.reset_index()
coefs = coefs.rename(columns={'index':'sensor_number'})

data = dataset.dfp.df
data = data.drop(data.iloc[:,9:].columns , axis=1)
data = data.drop(['datetime'], axis=1)
data = data.reset_index(drop=True)
dara = data.drop_duplicates()
total_coefs = pd.merge(dara, coefs, on='sensor_number')

# %%

data = total_coefs.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30],cmin=1)
axe.set_title('All sensor data, counts of coefficients values')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
_ = plt.hist(total_coefs.iloc[:,9:].values.ravel(),bins=50)

# %%
_ = plt.hist(total_coefs.iloc[:,9:].values.ravel(),bins=50,log=True)

# %% [markdown]
# Lets inspect this

# %%
filtered = total_coefs.iloc[:,9:].abs()>=0.02

# %%
filtered.columns[filtered.sum()!=0]

# %%
total_coefs[filtered.sum(axis=1)!=0].sensor_number

# %%
data = other["#6"].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[5,30]],cmin=1)
axe.set_title('Sensor #85, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
data = other.df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,100],cmin=1)
axe.set_title('All module data, pedestal distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
dfhp = dataset.dfh['phi'].df
super_inc = dfhp.iloc[:,9:]>300.0

# %%
super_high_values = dfhp[super_inc.sum(axis=1)>1]

# %%
super_high_values

# %%
data = dataset.dfh['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(2,1,figsize=(15,7))
_, xedges, yedges, _  = axe[0].hist2d(px, py, bins=[2048,30],cmin=1)
data = dataset.dfh['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
_, xedges, yedges, _   = axe[1].hist2d(px, py,range=[[xedges[0],xedges[-1]],[yedges[0],yedges[-1]]], bins=[2048,30],cmin=1)
axe[1].grid(linestyle='dashed')
axe[1].set_axisbelow(True)
axe[0].grid(linestyle='dashed')
axe[0].set_axisbelow(True)
axe[0].set_title('Phi-type module data, high threshold distribution per channel')
axe[0].set_xlabel('Channel number')
axe[0].set_ylabel('ADC')
axe[1].set_title('R-type module data, high threshold distribution per channel')
axe[1].set_xlabel('Channel number')
axe[1].set_ylabel('ADC')

fig.tight_layout()
fig.savefig(plot_output/"P2-threshold-all-r-phi.png")
fig.show()

# %%
exclusion = dataset.dfh['phi'].df
data = exclusion[(exclusion['datetime'] != '2016-11-07') & (exclusion['datetime'] != '2016-11-11') & (exclusion['sensor'] != 67.)]


data = data.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(2,1,figsize=(15,7))
_, xedges, yedges, _  = axe[0].hist2d(px, py, bins=[2048,30],cmin=1)
data = dataset.dfh['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
_, xedges, yedges, _   = axe[1].hist2d(px, py,range=[[xedges[0],xedges[-1]],[yedges[0],yedges[-1]]], bins=[2048,30],cmin=1)
axe[1].grid(linestyle='dashed')
axe[1].set_axisbelow(True)
axe[0].grid(linestyle='dashed')
axe[0].set_axisbelow(True)
axe[0].set_title('Phi-type module data, high threshold distribution per channel')
axe[0].set_xlabel('Channel number')
axe[0].set_ylabel('ADC')
axe[1].set_title('R-type module data, high threshold distribution per channel')
axe[1].set_xlabel('Channel number')
axe[1].set_ylabel('ADC')

fig.tight_layout()
fig.savefig(plot_output/"P2-threshold-all-r-phi-no-400.png")
fig.show()

# %%
exclusion = dataset.dfh['phi'].df
data = exclusion[(exclusion['datetime'] != '2016-11-07') & (exclusion['datetime'] != '2016-11-11') & (exclusion['sensor'] != 67.)]


data = data.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_, xedges, yedges, _  = axe.hist2d(px, py,range=[[450,500],[0,120]],cmin=1)
data = dataset.dfh['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)

axe.grid(linestyle='dashed')
axe.set_axisbelow(True)
axe.set_title('Phi-type module data, high threshold distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

fig.tight_layout()
fig.savefig(plot_output/"P2-phi-no-400.png")
fig.show()

# %%
dfhp = dataset.dfh['phi'].df
inc = dfhp[((dfhp.iloc[:,499:509]<100.0).sum(axis=1)>1) & ((dfhp.iloc[:,499:509]>30.0).sum(axis=1)>1)]

# %%
inc

# %%
xedges

# %%
data = dataset.dfh['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(2,1,figsize=(15,7))
_, xedges, yedges, _  = axe[0].hist2d(px, py,range=[[0,2047],[0,30]], bins=[2048,30],cmin=1)
data = dataset.dfh['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
_, xedges, yedges, _   = axe[1].hist2d(px, py,range=[[0,2047],[0,30]], bins=[2048,30],cmin=1)
axe[1].grid(linestyle='dashed')
axe[1].set_axisbelow(True)
axe[0].grid(linestyle='dashed')
axe[0].set_axisbelow(True)
axe[0].set_title('Phi-type module data, high threshold distribution per channel')
axe[0].set_xlabel('Channel number')
axe[0].set_ylabel('ADC')
axe[1].set_title('R-type module data, high threshold distribution per channel')
axe[1].set_xlabel('Channel number')
axe[1].set_ylabel('ADC')

fig.tight_layout()
fig.savefig(plot_output/"P2-threshold-all-zoom.png")
fig.show()

# %%
data = dataset.dfh['phi'].df.iloc[:,9:]
data = remove_anomalies(data, cut_val=None)
px, py = sensor_histogram(data)
fig, axe = plt.subplots(2,1,figsize=(15,7))
_, xedges, yedges, _  = axe[0].hist2d(px, py,range=[[0,2047],[0,30]], bins=[2048,30],cmin=1)
data = dataset.dfh['R'].df.iloc[:,9:]
data = remove_anomalies(data, cut_val=None)
px, py = sensor_histogram(data)
_, xedges, yedges, _   = axe[1].hist2d(px, py,range=[[0,2047],[0,30]], bins=[2048,30],cmin=1)
axe[1].grid(linestyle='dashed')
axe[1].set_axisbelow(True)
axe[0].grid(linestyle='dashed')
axe[0].set_axisbelow(True)
axe[0].set_title('Phi-type module data, high threshold distribution per channel')
axe[0].set_xlabel('Channel number')
axe[0].set_ylabel('ADC')
axe[1].set_title('R-type module data, high threshold distribution per channel')
axe[1].set_xlabel('Channel number')
axe[1].set_ylabel('ADC')

fig.tight_layout()
fig.savefig(plot_output/"P2-threshold-all-zoom-nohc.png")
fig.show()

# %%

# %%
data = wrong['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(2,1,figsize=(15,7))
_ = axe[0].hist2d(px, py, bins=[2048,25], range=[[0,2048],[5,30]],cmin=1)
axe[0].set_title('Bad callibration histogram, R-type module data, high threshold distribution per channel')
axe[0].set_xlabel('Channel number')
axe[0].set_ylabel('ADC')
data = wrong['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)

_ = axe[1].hist2d(px, py, bins=[2048,25], range=[[0,2048],[5,30]],cmin=1)
axe[1].set_title('Bad callibration histogram, Phi-type module data, high threshold distribution per channel')
axe[1].set_xlabel('Channel number')
axe[1].set_ylabel('ADC')
axe[1].grid(linestyle='dashed')
axe[1].set_axisbelow(True)
axe[0].grid(linestyle='dashed')
axe[0].set_axisbelow(True)
fig.tight_layout()

# %%
data = other['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(2,1,figsize=(15,7))
_ = axe[0].hist2d(px, py, bins=[2048,25], range=[[0,2048],[5,30]],cmin=1)
axe[0].set_title('Good callibration histogram, R-type module data, high threshold distribution per channel')
axe[0].set_xlabel('Channel number')
axe[0].set_ylabel('ADC')
data = other['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)

_ = axe[1].hist2d(px, py, bins=[2048,25], range=[[0,2048],[5,30]],cmin=1)
axe[1].set_title('Good callibration histogram, Phi-type module data, high threshold distribution per channel')
axe[1].set_xlabel('Channel number')
axe[1].set_ylabel('ADC')
axe[1].grid(linestyle='dashed')
axe[1].set_axisbelow(True)
axe[0].grid(linestyle='dashed')
axe[0].set_axisbelow(True)
fig.tight_layout()

# %% [markdown]
# Good

# %%
data = other['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,25], range=[[0,2048],[5,30]],cmin=1)
axe.set_title('Filtered histogram, R-type module data, high threshold distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %% [markdown]
# good

# %%
data = other['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]],cmin=1)
axe.set_title('Filtered histogram, Phi-type module data, high threshold distribution per channel')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')

# %%
from processing import BADCALS
from processing import remove_anomalies
fig, axe = plt.subplots(4,1,figsize=(15,14))
for i,(date, value) in enumerate(reversed(BADCALS.items())):
    data = DS.DatasetTree.from_df(dataset.dfh.df[dataset.dfh.df['datetime'] == date])
    data = data['R'].df.iloc[:,9:]
    data = remove_anomalies(data, cut_val=None)
    px, py = sensor_histogram(data)
    _ = axe[i].hist2d(px, py, bins=[2048,25], range=[[0,2048],[5,30]],cmin=1)
    axe[i].set_title('{}, R-type module data, high threshold distribution per channel, X={}'.format(date,value))
    axe[i].set_xlabel('Channel number')
    axe[i].set_ylabel('ADC')

fig.tight_layout()
fig.savefig(plot_output/"P2-all-bad-cals-R.png")

# %%
data = DS.DatasetTree.from_df(dataset.dfh.df[~dataset.dfh.df['datetime'].isin(BADCALS.keys())])
data = data['R'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]],cmin=1)
axe.set_title('Filtered histogram, R-type module data, high threshold distribution per channel, X=0')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')
fig.savefig(plot_output/"P2-only-good-R.png")

# %%
from processing import BADCALS
fig, axe = plt.subplots(4,1,figsize=(15,14))
for i,(date, value) in enumerate(reversed(BADCALS.items())):
    data = DS.DatasetTree.from_df(dataset.dfh.df[dataset.dfh.df['datetime'] == date])
    data = data['phi'].df.iloc[:,9:]
    px, py = sensor_histogram(data)
    _ = axe[i].hist2d(px, py, bins=[2048,25], range=[[0,2048],[5,30]],cmin=1)
    axe[i].set_title('{}, Phi-type module data, high threshold distribution per channel, X={}'.format(date,value))
    axe[i].set_xlabel('Channel number')
    axe[i].set_ylabel('ADC')

fig.tight_layout()
fig.savefig(plot_output/"P2-all-bad-cals-phi.png")

# %%
data = DS.DatasetTree.from_df(dataset.dfh.df[~dataset.dfh.df['datetime'].isin(BADCALS.keys())])
data = data['phi'].df.iloc[:,9:]
px, py = sensor_histogram(data)
fig, axe = plt.subplots(1,1,figsize=(15,3))
_ = plt.hist2d(px, py, bins=[2048,30], range=[[0,2048],[0,30]],cmin=1)
axe.set_title('Filtered histogram, Phi-type module data, high threshold distribution per channel, X=0')
axe.set_xlabel('Channel number')
axe.set_ylabel('ADC')
fig.savefig(plot_output/"P2-only-good-phi.png")

# %%
